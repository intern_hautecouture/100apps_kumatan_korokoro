﻿using NendUnityPlugin.AD.Video;
using NendUnityPlugin.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NendAdHintAd : MonoBehaviour {

    [Header("Hint Ad Config")]
    [SerializeField]
    NendUtils.Account account;

    public NendAdRewardedVideo rewardHintAd;

    private static NendAdHintAd _instance = null;

    public static NendAdHintAd Instance
    {
        get
        {
            return _instance;
        }
    }

    private void Awake()
    {
        if (null == _instance)
        {
            _instance = this;
            string spotId = "";
            string apiKey = "";

#if UNITY_IOS
        spotId = account.iOS.spotID.ToString();
        apiKey = account.iOS.apiKey;
#elif UNITY_ANDROID
            spotId = account.android.spotID.ToString();
            apiKey = account.android.apiKey;
#endif

            rewardHintAd = NendAdRewardedVideo.NewVideoAd(spotId, apiKey);
            rewardHintAd.AdCompleted += (instance) =>
             {
                 instance.Load();
             };
            rewardHintAd.Load();

        }
        else
        {
            Destroy(gameObject);
        }
    }

   
}
