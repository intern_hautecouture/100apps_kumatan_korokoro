﻿using NendUnityPlugin.AD;
using UnityEngine;

public class LevelButtonController : MonoBehaviour
{

    public bool isLocked;

    public void HandleOnClick()
    {
        if (!isLocked)
        {
            GameManager.levelLoaded = int.Parse(GetComponentInChildren<UnityEngine.UI.Text>().text);
            SgLib.SoundManager.Instance.PlaySound(SgLib.SoundManager.Instance.button);
            LevelScroller.levelSnapped = (int)((Mathf.Ceil(GameManager.levelLoaded / (float)LevelScroller.LEVELS_PER_PACK) - 1) * LevelScroller.LEVELS_PER_PACK + 1);

            GameObject.Find("NendAdBanner").GetComponent<NendAdBanner>().HideAdBannerAfterTime(0.4f);

            UnityEngine.SceneManagement.SceneManager.LoadScene("GameScene");        
        }     
    }
}
