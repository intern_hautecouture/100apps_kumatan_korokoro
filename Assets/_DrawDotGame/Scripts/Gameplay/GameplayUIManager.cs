﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SgLib;
using UnityEngine.SceneManagement;
using NendUnityPlugin.AD;
using Facebook.Unity;
using System.Collections.Generic;
using NendUnityPlugin.Common;
using NendUnityPlugin.AD.Video;

public class GameplayUIManager : MonoBehaviour
{
    public GameManager gameManager;
    public Text heartNumber;
    public Text levelText;
    public GameObject gameEndUI;
    public GameObject btnNext;
    public GameObject btnRetry;
    //public GameObject btnRestart;
    public GameObject btnHint;
    //public GameObject btnBack;
    public GameObject btnShareSocialMedia;
    public GameObject btnShareFacebook;
    public GameObject btnShareLine;
    public GameObject btnShareTwitter;
    public Image screenshotImg;
    public Image imgSolved;
    public Image imgFail;
    public Image blackPanel;
    public GameObject hintAlert;
    //public GameObject heartShortageAlert;
    public AnimationClip showMenuPanel;
    public AnimationClip hideMenuPanel;

    private bool hasWatchedAd;
    private bool isFirstWin;
    private const string HINT_ALERT_PPKEY = "SGLIB_HINT_ALERT";
    public static int levelTries = 0;
    public static int currLevel = 0;

    private Transform[] shareButtons;

    void OnEnable()
    {
        GameManager.GameEnded += OnGameEnded;
    }

    void OnDisable()
    {
        GameManager.GameEnded -= OnGameEnded;
    }

    // Use this for initialization
    void Start()
    {
        Debug.Log("START GAMEUIMANG");
        Debug.Log("level tries: " + levelTries);
        Debug.Log("currlevel. " + currLevel);

		//Load Interstitial AD
		NendAdInterstitial.Instance.Load ();

        if (currLevel != GameManager.levelLoaded)
        {
            currLevel = GameManager.levelLoaded;
            levelTries = 0;
        }
        if (levelTries == 3)
        {
            btnHint.SetActive(true);
            levelTries = 0;
        }

        blackPanel.gameObject.SetActive(false);
        gameEndUI.SetActive(false);
        levelText.text = "LEVEL " + GameManager.levelLoaded;
        //btnHint.SetActive(true);
        //btnRestart.SetActive(true);

        // Hidden at start
        hintAlert.SetActive(false);

        Debug.Log(btnShareFacebook);
        shareButtons = new Transform[] { btnShareFacebook.transform, btnShareLine.transform, btnShareTwitter.transform };
    }

    // Update is called once per frame
    void Update()
    {
        //
    }

    void OnGameEnded(bool isWin, bool firstWin)
    {
        if (isWin)
        {
            isFirstWin = firstWin;
            Invoke("ShowUIWhenWin", 1f);
        }
        else
        {
            Invoke("ShowUIWhenGameOver", 1f);
        }
    }

    void ShowUIWhenGameOver()
    {
        
        gameEndUI.SetActive(true);
        btnShareSocialMedia.SetActive(false);
        btnNext.SetActive(false);
        btnRetry.SetActive(true);
        blackPanel.gameObject.SetActive(true);
        imgFail.gameObject.SetActive(true);
        imgSolved.gameObject.SetActive(false);

        //Get the img and showing up
        DisplayScreenshot();
        if (Random.value < 0.3f)
        {
            NendAdInterstitial.Instance.Show();
        }
    }

    void ShowUIWhenWin()
    {
        
        // Award the user with hearts if needed
        if (isFirstWin && gameManager.heartsPerWin > 0)
        {
            CoinManager.Instance.AddCoins(gameManager.heartsPerWin);
            heartNumber.GetComponent<Animator>().Play("HeartIncrease");
            SoundManager.Instance.PlaySound(SoundManager.Instance.ping);
        }

        //Handle button
        blackPanel.gameObject.SetActive(true);
        gameEndUI.SetActive(true);
        btnShareSocialMedia.SetActive(true);

        int levelNumber = LevelScroller.maxLevelNumber;
        if (levelNumber == GameManager.levelLoaded)
        {
            Destroy(btnNext.transform.parent.gameObject);
            Destroy(btnNext);
            Destroy(btnRetry);
        }
        else
        {
            btnNext.SetActive(true);
            btnRetry.SetActive(false);
        }

        imgSolved.gameObject.SetActive(true);
        imgFail.gameObject.SetActive(false);

        //Get the img and showing up
        DisplayScreenshot();
        Debug.Log("Interstitial AD");
        if (Random.value < 0.3f)
        {
            Debug.Log("Show Interstitial on Win");
			NendAdInterstitial.Instance.Show();
        }
    }

    void DisplayScreenshot()
    {
        // Display the screenshot taken when game ends
        string filename = gameManager.win ? GameManager.levelLoaded.ToString() + ".png" : gameManager.failedScreenshotName;
        string path = System.IO.Path.Combine(Application.persistentDataPath, filename);
        byte[] bytes = System.IO.File.ReadAllBytes(path);
        Texture2D tex2D = new Texture2D(2, 2, TextureFormat.ARGB32, false);
        tex2D.LoadImage(bytes);
        float scaleFactor = screenshotImg.GetComponent<RectTransform>().rect.height / tex2D.height;
        screenshotImg.sprite = Sprite.Create(tex2D, new Rect(0, 0, tex2D.width, tex2D.height), Vector2.zero);
        screenshotImg.SetNativeSize();
        screenshotImg.transform.localScale = Vector3.one * scaleFactor;
    }

    public void GoToHome()
    {
        levelTries++;
        StartCoroutine(LoadScene("FirstScene"));
    }

    public void Retry()
    {
        levelTries++;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    //ヒントの表示Method
    //表示制限数は現在コメントアウト
    public void ShowHint()
    {
        if (gameManager.ShowHint())
        {
            //  if (PlayerPrefs.GetInt(HINT_ALERT_PPKEY, -99) == -99)
            //  {
            // Show hint alert.
            // hintAlert.SetActive(true);
            // hintAlert.GetComponentInChildren<Animator>().Play(showMenuPanel.name);

            // Overwrite pp key.
            PlayerPrefs.SetInt(HINT_ALERT_PPKEY, 1);
            //  }
            //btnHint.gameObject.SetActive(false);
        }
        /*  else
          {
              // Not enough hearts to show hint, show message here.
              heartShortageAlert.SetActive(true);
            //  heartShortageAlert.GetComponentInChildren<Animator>().Play(showMenuPanel.name);
          }*/
    }

    public void HideHintAlert()
    {
        StartCoroutine(CRHideHintAlert());
    }

    IEnumerator CRHideHintAlert()
    {
        hintAlert.GetComponentInChildren<Animator>().Play(hideMenuPanel.name);
        yield return new WaitForSeconds(hideMenuPanel.length);
        hintAlert.SetActive(false);
    }

    public void NextLevel()
    {
        /*int levelNumber = LevelScroller.maxLevelNumber;
        Debug.Log("level number: " + levelNumber);
        Debug.Log("level loaded: " + GameManager.levelLoaded);

        if (levelNumber == GameManager.levelLoaded)
        {
            GameManager.levelLoaded++;
            GoToHome();
        }
        else
        {*/
            GameManager.levelLoaded++;
            StartCoroutine(LoadScene(SceneManager.GetActiveScene().name));
        //}
    }

    IEnumerator LoadScene(string name)
    {
        yield return new WaitForSeconds(0.5f);

        SceneManager.LoadScene(name);
    }

    Vector3 originalPos = new Vector3(295, -111, 0);
    Vector3 posFacebook = new Vector3(475, -191, 1);
    Vector3 posTwitter = new Vector3(475, -111, 1);
    Vector3 posLine = new Vector3(475, -31, 1);
    bool buttonsOpened = false;

    public void ShareButton()
    {
        float transitionTime = 0.8f;

        CancelInvoke();
        Debug.Log(shareButtons);
        foreach (Transform button in shareButtons)
        {   
            //Debug.Log(button.name);
            var transitionVec = originalPos;

            button.GetComponent<Button>().interactable = false;

            if (!buttonsOpened)
            {
                button.gameObject.SetActive(true);

                button.GetComponent<Button>().interactable = true;

                Debug.Log("buttons opened");

                switch (button.name)
                {
                    case "FacebookButton":
                        transitionVec = posFacebook;
                        break;
                    case "TwitterButton":
                        transitionVec = posTwitter;
                        break;
                    case "LineButton":
                        transitionVec = posLine;
                        break;
                }
            }

            iTween.MoveTo(button.gameObject, iTween.Hash(
                "position", transitionVec,
                "isLocal", true,
                "time", transitionTime,
                "easyType", iTween.EaseType.linear
            ));
        }

        if (buttonsOpened) Invoke("HideButtons", 1f);

        buttonsOpened = !buttonsOpened;
    }

    public void HideButtons()
    {
        foreach (Transform button in btnShareSocialMedia.transform)
        {
            button.gameObject.SetActive(false);
        }
    }

    string clearedLevel = "";

    public void ShareTwitter()
    {
        string url = "https://play.google.com/store/apps/details?id=com.hautecouture.korokoro";

#if UNITY_ANDROID
        url = "https://play.google.com/store/apps/details?id=com.hautecouture.korokoro";
#else
        url = "http://urx3.nu/MmDT";
#endif

        //change this for this game
        string str1 = "クマタン コロコロコッツンでステージ";
        string str2 = "をクリア！\n";
        string str3 = " #クマタン";

        clearedLevel = GameManager.levelLoaded.ToString();

        Application.OpenURL("http://twitter.com/intent/tweet?text=" +
                            WWW.EscapeURL(str1 + clearedLevel + str2 + url + str3));
    }

    public void ShareLine()
    {
        string storeURL = "https://play.google.com/store/apps/details?id=com.hautecouture.korokoro";

#if UNITY_ANDROID
        storeURL = "https://play.google.com/store/apps/details?id=com.hautecouture.korokoro";
#else
        storeURL = "http://urx3.nu/MmDT";
#endif

        string str1 = "クマタン コロコロコッツンでステージ";
        string str2 = "をクリア！\n";

        clearedLevel = GameManager.levelLoaded.ToString();

        string msg = str1 + clearedLevel + str2 + storeURL;
        string url = "http://line.me/R/msg/text/?" + System.Uri.EscapeUriString(msg);
        Application.OpenURL(url);
    }

    public void ShareFacebook()
    {
        if (!FB.IsInitialized)
        {
            Debug.Log("FB Not Initialized");
            FB.Init(ShareLink);
        }
        else
        {
            //FB.ActivateApp();
            ShareLink();
        }
    }

    void ShareLink()
    {
		Debug.Log ("share link");
		Debug.Log("FB APP ID: " + FB.AppId);

		System.Uri FeedLink = new System.Uri("https://play.google.com/store/apps/details?id=com.hautecouture.korokoro");
#if UNITY_ANDROID
        FeedLink = new System.Uri("https://play.google.com/store/apps/details?id=com.hautecouture.korokoro");
#else
        FeedLink = new System.Uri("http://urx3.nu/MmDT");
#endif

        string FeedLinkName = "";
        string FeedLinkDescription = "";
        string str1 = "クマタン コロコロコッツン";
        string str2 = "クマタン コロコロコッツンでステージ";
        string str3 = "をクリア！";

        //TODO: change image when it is finished
        string pictureURL = "https://100apps.s3.amazonaws.com/000_Original/ShareImage/korokoro_head.jpg";

        FeedLinkName = str1;
        FeedLinkDescription = str2 + clearedLevel + str3;

		FB.ShareLink(FeedLink, FeedLinkName, FeedLinkDescription, new System.Uri(pictureURL), callback:HandleShareResult);
    }

	public void HandleShareResult(IResult result)
	{
		Debug.Log ("RESULT: " + result.ToString ());
	}

    public void WatchHintAd()
    {
        Invoke("HideHintAlert", 0.1f);
        gameManager.ShowHintAd();
        hintAlert.SetActive(false);
    }
}
